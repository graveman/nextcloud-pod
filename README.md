# Configuration
Rename `example.*` files removing the `example.` prefix and set their variables to your needs.

# Build image
This image includes cron support which handles e.g. database clean-ups.
See https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/background_jobs_configuration.html.
If you do not need this you can use the base image instead (e.g. docker.io/library/nextcloud:23.0.2-fpm-alpine).
`cd nextcloud`
`podman build --tag localhost/nextcloud:1.0.0-fpm-alpine .`

# Usage
podman play kube --configmap=postgres.yaml --configmap=collabora.yaml nextcloud-pod.yaml

# TODO check if this is true
NEXTCLOUD_TRUSTED_DOMAINS isnt working so add the host without port to the list manually
 - you will need to add the IP of the host machine in case proxy-pod is used.

# Making changes to the web server configuration
Note that the web container does not use volumes. You should add your changes to the files in
the my-httpd directory and rebuild the image.
You can use `podman cp <container>:<path> <local path>` and then diff the change.
Then rebuild (in the my-httpd directory):
`podman build --tag localhost/my-httpd:1.0-alpine .`

# Moving to a different server
If you are using the passwords app follow the migration instructions:
https://git.mdns.eu/nextcloud/passwords/-/wikis/Administrators/Guides/Maintenance/Server-Migration

# Upgrading
Warning: Nextcloud will automatically upgrade from a certain version (>18). You may (untested!) be able to
disable this feature by setting config_is_read_only to true. See https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/config_sample_php_parameters.html

In the nextcloud-pod(-mariadb).yaml file, replace the image of the app with your current version.
Ensure this runs properly.
The examples that follow use mariadb (see `example.nextcloud-pod-mariadb.yaml`).
Assuming you did not make any changes to the containers (which will be lost if you do not
add them to a volume) next remove the entire pod:
`podman pod rm -f nextcloud-maria`
Increment the image in the yaml file with one major version (e.g. nextcloud:19.0-fpm-alpine to nextcloud:20.0-fpm-alpine)
and then recreate the pod, which should pull in the new (and next) image (and version) of nextcloud:
`podman play kube --configmap=mariadb.yaml --configmap=collabora.yaml nextcloud-pod-mariadb.yaml`
If Nextcloud automatically updates, wait for a few minutes for it to finish.
Check that nextcloud is running (e.g. by visting the URL) and take note of the message that appears there.
Alternatively you can check the status with the following command:
`podman exec -u www-data -it nextcloud-maria-app ./occ status`
Assuming you've made a backup of your installation (see backup.sh) proceed with the upgrade:
`podman exec -u www-data -it nextcloud-maria-app ./occ upgrade`
Disable maintenance mode:
`podman exec -u www-data -it nextcloud-maria-app ./occ maintenance:mode --off`
You can now check if the upgrade went well by visiting and checking your instance.
A good place to check is the Settings/Administration/Overview page.
It may be a good idea to also update your apps (untested!).
Repeat until you reach your desired version.

# Converting database type
See https://docs.nextcloud.com/server/19/admin_manual/configuration_database/db_conversion.html
```
db:convert-type [--port PORT] [--password PASSWORD] [--clear-schema] [--all-apps] [--chunk-size CHUNK-SIZE] [--] <type> <username> <hostname> <database>
# Example:
podman exec -u www-data -it <container name> /var/www/html/occ db:convert-type --port="5432" --all-apps pgsql nextcloud nextcloud-maria nextcloud
```

# Using SSL
You can use a reverse proxy which provides SSL (see for example the proxy project here).
Otherwise you can use the `example.my-httpd-ssl.conf` in the my-httpd directory. You will need to generate certificates.
See https://letsencrypt.org/docs/certificates-for-localhost/ for an example. And you will need to adjust `my-httpd.conf`
to include `my-httpd-ssl.conf` and enable the modules specified in the example.

# To check
##  Does it install behind a reverse proxy?
https://docs.nextcloud.com/server/23/admin_manual/configuration_server/reverse_proxy_configuration.html

# Limitations
- No finetuning
- No security optimizations
- No explicit license
- Does not yet state proper attributions (Apache httpd, Podman, Nextcloud, Collabora)
- Hobby project (no support, no guarantees whatsoever)
