#!/bin/sh
PODNAME=nextcloud
# TODO change to external backup
REPO_PATH=<PATH_TO_BORG_REPO>
VOLUMES='nextcloud db'

echo Will prompt you for the repo password later on...
echo Creating big file to delete in case you run out of storage space.
echo Checking for existance of file, but not its size
echo
if [ ! -e delete-when-no-disk-space ]; then
    dd if=/dev/zero of=delete-when-no-disk-space bs=1G count=4
fi
echo
echo Stopping pod
podman pod stop $PODNAME
borg init --encryption=repokey-blake2 $REPO_PATH
echo
echo Exporting volumes
for VOLUME in $VOLUMES; do
    podman volume export -o volume-$VOLUME $VOLUME
done
# TODO check backup drive
#  mount
#  storage available
#  create big file anyway
echo
echo After this you will be prompted for the repo password TWICE. Once for the backup once for pruning
borg create --progress --stats ${REPO_PATH}::nextcloud-{utcnow} volume-nextcloud volume-db
echo
echo Cleaning up
rm volume-db volume-nextcloud
echo
echo Starting pod
podman pod start $PODNAME
# TODO prune if available space is less than 2 or 3 backups
borg prune --progress --stats --keep-daily 4 --keep-weekly 2 --keep-monthly 2 --keep-yearly 2 --save-space ${REPO_PATH}
